This module actually comes as a helper, to assist developers who wish to create
a custom form with image uploads preview, using the form API.
It is more suitable for the examples project and really after understanding
the code you may snippet to your module the relevant parts.


Installation
------------
Follow the standard contributed module installation process:
http://drupal.org/documentation/install/modules-themes/modules-7

* Configure the image style desired at admin/config/media/fapi-imagefield
* Example form at fapi-imagefield/example


Requirements
------------
Image module - Drupal 7 Core.


Configuration
-------------
(optional)
Just go to the Form API Image Field settings, under config/user-interface.
- Choose the image style you wish to use for previews.

All you need to do to use this module is to add a managed_file field and apply
the #theme attribute value "fapi_image_preview"

Example (Minimal):
  $form['imagefield'] = array(
    '#type' => 'managed_file',
    '#upload_location' => 'public://YOUR_PATH/',
    '#theme' => 'fapi_image_preview'
  );


Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/fapi_imagefield
